// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <armadillo>

#include "distmesh2d.hh"
#include "rat/common/gmshfile.hh"
#include "dfpolygon.hh"
#include "rat/common/serializer.hh"

// main
int main(){
	// settings
	const rat::fltp h0 = RAT_CONST(0.08);

	// create mesh
	rat::dm::ShDistMesh2DPr mymesh = rat::dm::DistMesh2D::create();

	// initial edge length
	mymesh->set_h0(h0);
	mymesh->set_quad(true);

	arma::Mat<rat::fltp> v(10,2);
	v.row(0) = arma::Row<rat::fltp>::fixed<2>{-0.4,-0.5};
	v.row(1) = arma::Row<rat::fltp>::fixed<2>{0.4,-0.2};
	v.row(2) = arma::Row<rat::fltp>::fixed<2>{0.4,-0.7};
	v.row(3) = arma::Row<rat::fltp>::fixed<2>{1.5,-0.4};
	v.row(4) = arma::Row<rat::fltp>::fixed<2>{0.9,0.1};
	v.row(5) = arma::Row<rat::fltp>::fixed<2>{1.6,0.8};
	v.row(6) = arma::Row<rat::fltp>::fixed<2>{0.5,0.5};
	v.row(7) = arma::Row<rat::fltp>::fixed<2>{0.2,1};
	v.row(8) = arma::Row<rat::fltp>::fixed<2>{0.1,0.4};
	v.row(9) = arma::Row<rat::fltp>::fixed<2>{-0.7,0.7};

	// the distance function
	mymesh->set_distfun(rat::dm::DFPolygon::create(v));
	
	// setup mesh
	mymesh->setup();

	// get mesh
	arma::Mat<arma::uword> q = mymesh->get_elements();
	arma::Mat<rat::fltp> p = mymesh->get_nodes();
	arma::Mat<arma::uword> es = mymesh->get_surface_edges();

	// write to gmsh
	mymesh->export_gmsh(rat::cmn::GmshFile::create("poly.gmsh"));

	// write to output file
	rat::cmn::ShSerializerPr slzr = rat::cmn::Serializer::create();
	slzr->flatten_tree(mymesh); slzr->export_json("poly.json");

	return 0;
}