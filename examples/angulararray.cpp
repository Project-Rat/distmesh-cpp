// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general headers
#include <armadillo>

// common headers
#include "rat/common/gmshfile.hh"
#include "rat/common/serializer.hh"

// distmesher
#include "distmesh2d.hh"
#include "dfcircle.hh"
#include "dfrectangle.hh"
#include "dfscale.hh"
#include "dfdiff.hh"
#include "dfarrayangular.hh"

// main
int main(){
	// settings
	const arma::uword num_sym = 6;
	const arma::uword num_array = 4;
	const rat::fltp array_radius = RAT_CONST(0.06);
	const rat::fltp radius = RAT_CONST(0.02);
	const rat::fltp element_size = 0.004;

	// create mesh
	rat::dm::ShDistMesh2DPr mymesh = rat::dm::DistMesh2D::create();

	// initial edge length
	mymesh->set_h0(element_size);

	// the distance function
	mymesh->set_distfun(rat::dm::DFArrayAngular::create(
		rat::dm::DFCircle::create(radius,0,0),num_sym,num_array,array_radius,arma::Datum<rat::fltp>::tau/(2*num_sym)));

	// setup mesh
	mymesh->setup();

	// write to gmsh
	mymesh->export_gmsh(rat::cmn::GmshFile::create("angular_array.gmsh"));
	mymesh->export_gmsh_perimeter(rat::cmn::GmshFile::create("angular_array_perimeter.gmsh"));

	// write to output file
	rat::cmn::ShSerializerPr slzr = rat::cmn::Serializer::create();
	slzr->flatten_tree(mymesh); slzr->export_json("angular_array.json");

	return 0;
}