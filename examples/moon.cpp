// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <armadillo>

#include "rat/common/gmshfile.hh"
#include "rat/common/serializer.hh"
#include "rat/common/typedefs.hh"

#include "distmesh2d.hh"
#include "dfcircle.hh"
#include "dfdiff.hh"

// main
int main(){
	// settings
	const rat::fltp h0 = RAT_CONST(0.1);
	const rat::fltp r1 = RAT_CONST(1.0);
	const rat::fltp xc1 = RAT_CONST(0.0);
	const rat::fltp yc1 = RAT_CONST(0.0);
	const rat::fltp r2 = RAT_CONST(0.5);
	const rat::fltp xc2 = RAT_CONST(0.7);
	const rat::fltp yc2 = RAT_CONST(0.0);

	// create mesh
	rat::dm::ShDistMesh2DPr mymesh = rat::dm::DistMesh2D::create();

	// initial edge length
	mymesh->set_h0(h0);

	// the distance function
	mymesh->set_distfun(rat::dm::DFDiff::create(
		rat::dm::DFCircle::create(r1,xc1,yc1),
		rat::dm::DFCircle::create(r2,xc2,yc2)));
	
	// setup mesh
	mymesh->setup();

	// get mesh
	arma::Mat<arma::uword> q = mymesh->get_elements();
	arma::Mat<rat::fltp> p = mymesh->get_nodes();
	arma::Mat<arma::uword> es = mymesh->get_surface_edges();

	// write to gmsh
	mymesh->export_gmsh(rat::cmn::GmshFile::create("moon.gmsh"));

	// write to output file
	rat::cmn::ShSerializerPr slzr = rat::cmn::Serializer::create();
	slzr->flatten_tree(mymesh); slzr->export_json("moon.json");

	return 0;
}