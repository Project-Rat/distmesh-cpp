// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general headers
#include <armadillo>

// common headers
#include "rat/common/gmshfile.hh"
#include "rat/common/serializer.hh"

// distmesher
#include "distmesh2d.hh"
#include "dfcircle.hh"
#include "dfrectangle.hh"
#include "dfscale.hh"
#include "dfdiff.hh"
#include "dfarrayangular.hh"

// main
int main(){
	// settings
	const rat::fltp Rin = RAT_CONST(0.08);
	const rat::fltp Rout = RAT_CONST(0.2);
	const rat::fltp dchannel = RAT_CONST(0.04);

	// create mesh
	rat::dm::ShDistMesh2DPr mymesh = rat::dm::DistMesh2D::create();

	// initial edge length
	mymesh->set_h0(0.02);

	// the distance function
	mymesh->set_distfun(rat::dm::DFDiff::create({
		rat::dm::DFCircle::create(Rout,0,0),
		rat::dm::DFCircle::create(Rin,0,0),
		// rat::dm::DFRectangle::create(-dchannel/2,dchannel/2,0.17,0.21),
		// rat::dm::DFRectangle::create(-dchannel/2,dchannel/2,-0.21,-0.17),
		rat::dm::DFArrayAngular::create(rat::dm::DFRectangle::create(-dchannel/2,dchannel/2,0.17,0.21),2,2),
		rat::dm::DFArrayAngular::create(rat::dm::DFCircle::create((Rout-Rin)/4,0,0),4,4,(Rout+Rin)/2,arma::Datum<rat::fltp>::tau/8)}));

	// setup mesh
	mymesh->setup();

	// // get mesh
	// arma::Mat<arma::uword> q = mymesh->get_elements();
	// arma::Mat<rat::fltp> p = mymesh->get_nodes();
	// arma::Mat<arma::uword> es = mymesh->get_surface_edges();

	// write to gmsh
	mymesh->export_gmsh(rat::cmn::GmshFile::create("cyoke.gmsh"));
	mymesh->export_gmsh_perimeter(rat::cmn::GmshFile::create("cyoke_perimeter.gmsh"));

	// write to output file
	rat::cmn::ShSerializerPr slzr = rat::cmn::Serializer::create();
	slzr->flatten_tree(mymesh); slzr->export_json("cyoke.json");

	return 0;
}