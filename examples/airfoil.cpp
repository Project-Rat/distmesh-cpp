// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <armadillo>

#include "distmesh2d.hh"
#include "rat/common/gmshfile.hh"
#include "dfairfoil.hh"
#include "dfunion.hh"
#include "dfscale.hh"
#include "dfcircle.hh"
#include "dfdiff.hh"
#include "rat/common/serializer.hh"

// main
int main(){
	// settings
	const rat::fltp hlead=RAT_CONST(0.005); 
	const rat::fltp htrail=RAT_CONST(0.002); 
	const rat::fltp hmax=RAT_CONST(0.08);

	// create mesh
	rat::dm::ShDistMesh2DPr mymesh = rat::dm::DistMesh2D::create();

	// initial edge length
	mymesh->set_h0(std::min(std::min(hlead,htrail),hmax));

	// the distance function
	mymesh->set_distfun(rat::dm::DFAirFoil::create());
	
	// scaling function
	mymesh->set_scalefun(
		rat::dm::DFScale::create(rat::dm::DFUnion::create(
		rat::dm::DFScale::create(rat::dm::DFCircle::create(0,0,0),hlead,0.3),
		rat::dm::DFScale::create(rat::dm::DFCircle::create(0,1,0),htrail,0.3)),0,1.0,hmax));

	// setup mesh
	mymesh->setup();

	// get mesh
	const arma::Mat<arma::uword>& q = mymesh->get_elements();
	const arma::Mat<rat::fltp>& p = mymesh->get_nodes();
	arma::Mat<arma::uword> es = mymesh->get_surface_edges();

	// write to gmsh
	mymesh->export_gmsh(rat::cmn::GmshFile::create("airfoil.gmsh"));
	
	// write to output file
	rat::cmn::ShSerializerPr slzr = rat::cmn::Serializer::create();
	slzr->flatten_tree(mymesh); slzr->export_json("airfoil.json");

	return 0;
}