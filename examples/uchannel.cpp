// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <armadillo>

#include "distmesh2d.hh"
#include "rat/common/gmshfile.hh"
#include "dfpolygon.hh"
#include "rat/common/serializer.hh"

// main
int main(){
	// settings
	const rat::fltp h0 = std::sqrt(RAT_CONST(2.0))*RAT_CONST(1e-3);

	std::cout<<h0<<std::endl;
	// create mesh
	rat::dm::ShDistMesh2DPr mymesh = rat::dm::DistMesh2D::create();

	// initial edge length
	mymesh->set_h0(h0);
	mymesh->set_quad(true);

	arma::Mat<rat::fltp> v(9,2);
	v.row(0) = arma::Row<rat::fltp>::fixed<2>{0,0.009};
	v.row(1) = arma::Row<rat::fltp>::fixed<2>{2.735724288274e-03,0.009};
	v.row(2) = arma::Row<rat::fltp>::fixed<2>{2.735724288274e-03,-0.009};
	v.row(3) = arma::Row<rat::fltp>::fixed<2>{0,-0.009};
	v.row(4) = arma::Row<rat::fltp>::fixed<2>{0,-0.006};
	v.row(5) = arma::Row<rat::fltp>::fixed<2>{6.300791166972e-04,-0.006};
	v.row(6) = arma::Row<rat::fltp>::fixed<2>{6.300791166972e-04,0.006};
	v.row(7) = arma::Row<rat::fltp>::fixed<2>{0,0.006};
	v.row(8) = arma::Row<rat::fltp>::fixed<2>{0,0.009};

	// the distance function
	mymesh->set_distfun(rat::dm::DFPolygon::create(v));
	
	// setup mesh
	mymesh->setup();

	// get mesh
	arma::Mat<arma::uword> q = mymesh->get_elements();
	arma::Mat<rat::fltp> p = mymesh->get_nodes();
	arma::Mat<arma::uword> es = mymesh->get_surface_edges();

	// write to gmsh
	mymesh->export_gmsh(rat::cmn::GmshFile::create("uchannel.gmsh"));

	// write to output file
	rat::cmn::ShSerializerPr slzr = rat::cmn::Serializer::create();
	slzr->flatten_tree(mymesh); slzr->export_json("uchannel.json");

	return 0;
}