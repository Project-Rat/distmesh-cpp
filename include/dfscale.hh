// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef DM_DF_SCALE_HH
#define DM_DF_SCALE_HH

// general headers
#include <armadillo> 
#include <cassert>
#include <memory>
#include <json/json.h>

// distmesh headers
#include "distfun.hh"

// code specific to Rat
namespace rat{namespace dm{

	// shared pointer definition
	typedef std::shared_ptr<class DFScale> ShDFScalePr;

	// circle distance function for distmesh
	class DFScale: public DistFun{
		// properties
		private:
			// scaling
			fltp offset_ = RAT_CONST(0.0);
			fltp scale_ = RAT_CONST(1.0);
			fltp max_ = RAT_CONST(1e5);

			// boundaries
			ShDistFunPr df_;

		// methods
		public:
			// constructcor
			DFScale();
			DFScale(
				const ShDistFunPr &df, 
				const fltp offset, 
				const fltp scale, 
				const fltp max = 1e99);

			// factory
			static ShDFScalePr create();
			static ShDFScalePr create(
				const ShDistFunPr &df, 
				const fltp offset, 
				const fltp scale, 
				const fltp max = arma::datum::inf);

			// setters
			void set_df(const ShDistFunPr &df);
			void set_offset(const fltp offset);
			void set_scale(const fltp scale);
			void set_max(const fltp max);

			// getters
			const ShDistFunPr& get_df()const;
			fltp get_offset()const;
			fltp get_scale()const;
			fltp get_max()const;

			// get bounding box
			virtual arma::Mat<fltp>::fixed<2,2> get_bounding() const override;

			// fixed points
			virtual arma::Mat<fltp> get_fixed(const fltp abstol) const override;

			// distance function
			virtual arma::Col<fltp> calc_distance(const arma::Mat<fltp> &p) const override;

			// validity check
			virtual bool is_valid(const bool enable_throws)const override;

			// serialization
			static std::string get_type(); 
			void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
