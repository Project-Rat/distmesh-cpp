// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef DM_PERIMETER_HH
#define DM_PERIMETER_HH

#include <armadillo> 
#include <cassert>
#include <memory>
#include <list>

// common headers
#include "rat/common/typedefs.hh"

// code specific to Rat
namespace rat{namespace dm{

	// shared pointer definition
	typedef std::shared_ptr<class Perimeter> ShPerimeterPr;

	// circle distance function for distmesh
	class Perimeter{
		// properties
		private:
			// node matrix
			arma::Mat<fltp> Rn_;

			// element matrix
			arma::Mat<arma::uword> n_;

		// methods
		public:
			// constructcor
			explicit Perimeter(
				const std::list<ShPerimeterPr>&perimeters);
			Perimeter(
				const arma::Mat<fltp>&Rn, 
				const arma::Mat<arma::uword>&n);

			// factory
			static ShPerimeterPr create(
				const std::list<ShPerimeterPr>&perimeters);
			static ShPerimeterPr create(
				const arma::Mat<fltp>&Rn, 
				const arma::Mat<arma::uword>&n);

			// getters
			const arma::Mat<fltp>& get_nodes()const;

			// setters
			const arma::Mat<arma::uword>& get_elements()const;

			// intersection function
			arma::Mat<fltp> intersect_df(
				const std::shared_ptr<class DistFun>& df);

			// static void refine_intersection(
			// 	arma::Mat<fltp> &Pi, 
			// 	const std::shared_ptr<class DistFun>& df1, 
			// 	const std::shared_ptr<class DistFun>& df2,
			// 	const fltp delta = 1e-7,
			// 	const fltp abstol = 1e-7);

			// static void drop_lowgradangle(
			// 	arma::Mat<fltp> &Pi, 
			// 	const std::shared_ptr<class DistFun>& df1, 
			// 	const std::shared_ptr<class DistFun>& df2,
			// 	const fltp delta = 1e-7);
	};

}}

#endif
