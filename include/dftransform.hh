// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef DM_DF_TRANSFORM_HH
#define DM_DF_TRANSFORM_HH

// general headers
#include <armadillo> 
#include <cassert>
#include <memory>
#include <json/json.h>

// distmesh headers
#include "distfun.hh"

// code specific to Rat
namespace rat{namespace dm{

	// shared pointer definition
	typedef std::shared_ptr<class DFTransform> ShDFTransformPr;

	// circle distance function for distmesh
	class DFTransform: public DistFun{
		// properties
		private:
			// boundaries
			ShDistFunPr df_;

			// move distance
			arma::Col<fltp>::fixed<2> dR_{RAT_CONST(0.0),RAT_CONST(0.0)};

			// rotate (happens after moving)
			fltp alpha_ = RAT_CONST(0.0);

		// methods
		public:
			// constructcor
			DFTransform();
			DFTransform(
				const ShDistFunPr& df,
				const arma::Col<fltp>::fixed<2>& dR, 
				const fltp alpha);

			// factory
			static ShDFTransformPr create();
			static ShDFTransformPr create(
				const ShDistFunPr& df,
				const arma::Col<fltp>::fixed<2>& dR, 
				const fltp alpha);

			// setters
			void set_df(const ShDistFunPr &df);
			void set_offset(const arma::Col<fltp>::fixed<2>& dR);
			void set_offset_x(const fltp dx);
			void set_offset_y(const fltp dy);
			void set_alpha(const fltp alpha);

			// getters
			const ShDistFunPr& get_df()const;
			const arma::Col<fltp>::fixed<2>& get_offset()const;
			fltp get_alpha()const;
			fltp get_offset_x()const;
			fltp get_offset_y()const;

			// perimeter function
			virtual ShPerimeterPr create_perimeter(const fltp delem)const override;

			// get bounding box
			virtual arma::Mat<fltp>::fixed<2,2> get_bounding()const override;

			// fixed points
			virtual arma::Mat<fltp> get_fixed(const fltp abstol)const override;

			// distance function
			virtual arma::Col<fltp> calc_distance(const arma::Mat<fltp> &p)const override;

			// validity check
			virtual bool is_valid(const bool enable_throws)const override;

			// serialization
			static std::string get_type(); 
			void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
