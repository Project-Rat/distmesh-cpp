// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef DM_DF_ELLIPSE_HH
#define DM_DF_ELLIPSE_HH

#include <armadillo> 
#include <cassert>
#include <memory>
#include <json/json.h>

#include "distfun.hh"

// code specific to Rat
namespace rat{namespace dm{

	// shared pointer definition
	typedef std::shared_ptr<class DFEllipse> ShDFEllipsePr;

	// circle distance function for distmesh
	class DFEllipse: public DistFun{
		// properties
		private:
			// size and shape
			fltp a_ = RAT_CONST(0.1); 
			fltp b_ = RAT_CONST(0.05); 
			fltp xe_ = RAT_CONST(0.0); 
			fltp ye_ = RAT_CONST(0.0);

		// methods
		public:
			// constructcor
			DFEllipse();
			DFEllipse(
				const fltp a, const fltp b, 
				const fltp xe = RAT_CONST(0.0), 
				const fltp ye = RAT_CONST(0.0));

			// factory
			static ShDFEllipsePr create();
			static ShDFEllipsePr create(
				const fltp a, const fltp b, 
				const fltp xe = RAT_CONST(0.0), 
				const fltp ye = RAT_CONST(0.0));

			// setters
			void set_a(const fltp a);
			void set_b(const fltp b);
			void set_xe(const fltp xe);
			void set_ye(const fltp ye);

			// getters
			fltp get_a()const;
			fltp get_b()const;
			fltp get_xe()const;
			fltp get_ye()const;

			// get bounding box
			virtual arma::Mat<fltp>::fixed<2,2> get_bounding() const override;

			// distance function
			virtual arma::Col<fltp> calc_distance(const arma::Mat<fltp> &p) const override;

			// validity check
			virtual bool is_valid(const bool enable_throws)const override;

			// serialization
			static std::string get_type(); 
			void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
