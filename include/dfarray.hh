// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef DM_DF_ARRAY_HH
#define DM_DF_ARRAY_HH

#include <armadillo> 
#include <cassert>
#include <memory>
#include <json/json.h>

#include "distfun.hh"

// code specific to Rat
namespace rat{namespace dm{

	// shared pointer definition
	typedef std::shared_ptr<class DFArray> ShDFArrayPr;

	// circle distance function for distmesh
	class DFArray: public DistFun{
		// properties
		private:
			// centering
			bool is_centered_ = false;

			// array size
			arma::uword num_x_ = 1llu;
			arma::uword num_y_ = 1llu;

			// spacing
			fltp dx_ = RAT_CONST(0.0);
			fltp dy_ = RAT_CONST(0.0);

			// distance functions
			std::map<arma::uword,ShDistFunPr> df_;

		// methods
		public:
			// constructcor
			DFArray();
			DFArray(
				const ShDistFunPr& df, 
				const arma::uword num_x, 
				const arma::uword num_y, 
				const fltp dx, const fltp dy, 
				const bool is_centered = false);

			// factory
			static ShDFArrayPr create();
			static ShDFArrayPr create(
				const ShDistFunPr& df, 
				const arma::uword num_x, 
				const arma::uword num_y, 
				const fltp dx, const fltp dy, 
				const bool is_centered = false);
			
			// access to stored distance functions
			void add(const ShDistFunPr& df);
			arma::uword add_df(const ShDistFunPr& df);
			const ShDistFunPr& get_df(const arma::uword index) const;
			bool delete_df(const arma::uword index);
			arma::uword num_df() const;
			void reindex() override;

			// setters
			void set_is_centered(const bool is_centered = true);
			void set_num_x(const arma::uword num_x);
			void set_num_y(const arma::uword num_y);
			void set_dx(const fltp dx);
			void set_dy(const fltp dy);

			// getters
			bool get_is_centered()const;
			arma::uword get_num_x()const;
			arma::uword get_num_y()const;
			fltp get_dx()const;
			fltp get_dy()const;

			// create union distance function to represent the full array
			virtual ShDistFunPr create_array()const;

			// perimeter function
			virtual ShPerimeterPr create_perimeter(const fltp delem) const override;
			
			// get bounding box
			virtual arma::Mat<fltp>::fixed<2,2> get_bounding() const override;
				
			// fixed points
			virtual arma::Mat<fltp> get_fixed(const fltp abstol) const override;
				
			// distance function
			virtual arma::Col<fltp> calc_distance(const arma::Mat<fltp> &p) const override;

			// validity check
			virtual bool is_valid(const bool enable_throws)const override;

			// serialization
			static std::string get_type(); 
			void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
