// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef DM_DF_RRECTANGLE_HH
#define DM_DF_RRECTANGLE_HH

#include <armadillo> 
#include <cassert>
#include <memory>
#include <json/json.h>

#include "distfun.hh"
#include "dfrectangle.hh"
#include "dfcircle.hh"
#include "dfunion.hh"

// code specific to Rat
namespace rat{namespace dm{

	// shared pointer definition
	typedef std::shared_ptr<class DFRRectangle> ShDFRRectanglePr;

	// rounded rectangle distance function
	class DFRRectangle: public DistFun{
		// properties
		private:
			// radius
			fltp radius_ = RAT_CONST(5e-3);

			// boundaries
			fltp x1_ = -RAT_CONST(30e-3);
			fltp x2_ = RAT_CONST(30e-3);
			fltp y1_ = -RAT_CONST(30e-3);
			fltp y2_ = RAT_CONST(30e-3);

		// methods
		public:
			// constructcor
			DFRRectangle();
			DFRRectangle(
				const fltp x1, const fltp x2, 
				const fltp y1, const fltp y2, 
				const fltp radius);

			// factory
			static ShDFRRectanglePr create();
			static ShDFRRectanglePr create(
				const fltp x1, const fltp x2, 
				const fltp y1, const fltp y2, 
				const fltp radius);

			// setters
			void set_x1(const fltp x1);
			void set_x2(const fltp x2);
			void set_y1(const fltp y1);
			void set_y2(const fltp y2);
			void set_radius(const fltp radius);
			
			// getters
			fltp get_x1()const;
			fltp get_x2()const;
			fltp get_y1()const;
			fltp get_y2()const;
			fltp get_radius()const;

			// perimeter function
			virtual ShPerimeterPr create_perimeter(const fltp delem) const override;

			// fixed points
			virtual arma::Mat<fltp> get_fixed(const fltp abstol) const override;

			// get bounding box
			virtual arma::Mat<fltp>::fixed<2,2> get_bounding() const override;

			// distance function
			virtual arma::Col<fltp> calc_distance(const arma::Mat<fltp> &p) const override;

			// validity check
			virtual bool is_valid(const bool enable_throws)const override;

			// serialization
			static std::string get_type(); 
			void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
