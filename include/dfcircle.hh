// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef DM_DF_CIRCLE_HH
#define DM_DF_CIRCLE_HH

#include <armadillo> 
#include <cassert>
#include <memory>

#include <json/json.h>

#include "distfun.hh"

// code specific to Rat
namespace rat{namespace dm{

// shared pointer definition
typedef std::shared_ptr<class DFCircle> ShDFCirclePr;

	// circle distance function for distmesh
	class DFCircle: public DistFun{
		// properties
		private:
			// radius
			fltp radius_ = 0.025;

			// centre position
			fltp xc_ = 0.0;
			fltp yc_ = 0.0;

		// methods
		public:
			// constructcor
			DFCircle();
			DFCircle(const fltp radius, const fltp xc = 0.0, const fltp yc = 0.0);

			// factory
			static ShDFCirclePr create();
			static ShDFCirclePr create(const fltp radius, const fltp xc = 0.0, const fltp yc = 0.0);
			
			// setters
			void set_radius(const fltp radius);
			void set_xc(const fltp xc);
			void set_yc(const fltp yc);

			// getters
			fltp get_radius()const;
			fltp get_xc()const;
			fltp get_yc()const;
			
			// perimeter function
			virtual ShPerimeterPr create_perimeter(const fltp delem) const override;

			// get bounding box
			virtual arma::Mat<fltp>::fixed<2,2> get_bounding() const override;

			// distance function
			virtual arma::Col<fltp> calc_distance(const arma::Mat<fltp> &p) const override;

			// validity check
			virtual bool is_valid(const bool enable_throws)const override;

			// serialization
			static std::string get_type(); 
			void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			void deserialize(
				const Json::Value &js, cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override; 
	};

}}

#endif
