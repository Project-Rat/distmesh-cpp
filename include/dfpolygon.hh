// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef DM_DF_POLYGON_HH
#define DM_DF_POLYGON_HH

#include <armadillo> 
#include <cassert>
#include <memory>
#include <algorithm>
#include <json/json.h>

#include "distfun.hh"

// code specific to Rat
namespace rat{namespace dm{

	// shared pointer definition
	typedef std::shared_ptr<class DFPolygon> ShDFPolygonPr;

	// circle distance function for distmesh
	class DFPolygon: public DistFun{
		// properties
		private:
			// scale factor
			fltp scale_factor_ = RAT_CONST(1.0);

			// polygon points
			arma::Mat<fltp> v_;

		// methods
		public:
			// constructcor
			DFPolygon();
			explicit DFPolygon(const arma::Mat<fltp> &v);

			// factory
			static ShDFPolygonPr create();
			static ShDFPolygonPr create(const arma::Mat<fltp> &v);

			// setters
			void set_scale_factor(const fltp scale_factor);
			void set_coords(const arma::Mat<fltp> &v);

			// getters
			fltp get_scale_factor()const;
			const arma::Mat<fltp>& get_coords()const;

			// perimeter function
			virtual ShPerimeterPr create_perimeter(const fltp delem) const override;
			
			// get bounding box
			virtual arma::Mat<fltp>::fixed<2,2> get_bounding() const override;
					
			// fixed points
			virtual arma::Mat<fltp> get_fixed(const fltp abstol) const override;

			// check function to see if points are inside a polygon
			static arma::Col<arma::uword> inpolygon(const arma::Mat<fltp> &v, const arma::Mat<fltp> &p);

			// distance function
			virtual arma::Col<fltp> calc_distance(const arma::Mat<fltp> &p) const override;

			// calculate area used for validity check
			fltp calculate_area()const;

			// validity check
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type(); 
			void serialize(Json::Value &js, cmn::SList &list) const override;
			void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth) override;
	};

}}

#endif
