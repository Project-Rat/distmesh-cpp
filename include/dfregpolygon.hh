// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef DM_DF_REG_POLYGON_HH
#define DM_DF_REG_POLYGON_HH

#include <armadillo> 
#include <cassert>
#include <memory>
#include <algorithm>
#include <json/json.h>

#include "distfun.hh"

// code specific to Rat
namespace rat{namespace dm{

	// shared pointer definition
	typedef std::shared_ptr<class DFRegPolygon> ShDFRegPolygonPr;

	// circle distance function for distmesh
	class DFRegPolygon: public DistFun{
		// properties
		private:
			// radius of polygon corners
			fltp radius_ = RAT_CONST(0.0);

			// distance from origin to flat side of polygon
			fltp alpha_ = RAT_CONST(0.1); 

			// number of points
			arma::uword num_sides_ = 6llu;

		// methods
		public:
			// constructcor
			DFRegPolygon();
			DFRegPolygon(const arma::uword num_sides, const fltp alpha, const fltp radius = RAT_CONST(0.0));

			// factory
			static ShDFRegPolygonPr create();
			static ShDFRegPolygonPr create(const arma::uword num_sides, const fltp alpha, const fltp radius = RAT_CONST(0.0));

			// setters
			void set_radius(const fltp radius);
			void set_alpha(const fltp alpha);
			void set_num_sides(const arma::uword num_sides);

			// getters
			fltp get_radius()const;
			fltp get_alpha()const;
			arma::uword get_num_sides()const;

			// calculate important lengths
			fltp calc_side_length() const;
			fltp calc_arc_length() const;
			fltp calc_corner_length() const;

			// distance function of the combined objects
			ShDistFunPr create_distfun()const;

			// perimeter function
			virtual ShPerimeterPr create_perimeter(const fltp delem) const override;

			// fixed points
			virtual arma::Mat<fltp> get_fixed(const fltp abstol) const override;

			// get bounding box
			virtual arma::Mat<fltp>::fixed<2,2> get_bounding() const override;

			// distance function
			virtual arma::Col<fltp> calc_distance(const arma::Mat<fltp> &p) const override;

			// validity check
			virtual bool is_valid(const bool enable_throws)const override;
			
			// serialization
			static std::string get_type(); 
			void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif
