// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef DM_FACTORY_LIST_HH
#define DM_FACTORY_LIST_HH

#include "rat/common/node.hh"
#include "rat/common/serializer.hh"

#include "distmesh2d.hh"
#include "dfairfoil.hh"
#include "dfcircle.hh"
#include "dfdiff.hh"
#include "dfellipse.hh"
#include "dfintersect.hh"
#include "dfones.hh"
#include "dfpolygon.hh"
#include "dfrectangle.hh"
#include "dfrrectangle.hh"
#include "dfscale.hh"
#include "dfunion.hh"

// code specific to Rat
namespace rat{namespace mat{

	// use a class for storage
	class FactoryList{
		public:
			static void register_constructors(rat::cmn::ShSerializerPr slzr){
				// register all required constructors
				register_factory<rat::dm::DistMesh2D>();
				register_factory<rat::dm::DFAirFoil>();
				register_factory<rat::dm::DFCircle>();
				register_factory<rat::dm::DFDiff>();
				register_factory<rat::dm::DFEllipse>();
				register_factory<rat::dm::DFIntersect>();
				register_factory<rat::dm::DFOnes>();
				register_factory<rat::dm::DFPolygon>();
				register_factory<rat::dm::DFRectangle>();
				register_factory<rat::dm::DFRRectangle>();
				register_factory<rat::dm::DFScale>();
				register_factory<rat::dm::DFUnion>();
			}
	};

}}

#endif



