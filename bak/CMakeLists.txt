# compile the MLFMM library
file(GLOB SOURCE_FILES "*.cpp")

# make dynamic library
add_library(distmesh ${SOURCE_FILES})

# properties
set_target_properties(distmesh PROPERTIES VERSION ${PROJECT_VERSION})

# If we have compiler requirements for this library, list them here
target_compile_features(distmesh
    PUBLIC cxx_auto_type
    PRIVATE cxx_variadic_templates)

# Depend on a library that we defined in the top-level file
target_link_libraries(distmesh ${LIBS})