// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "dfunion.hh"

// common headers
#include "rat/common/extra.hh"

// code specific to Rat
namespace rat{namespace dm{

	// constructor
	DFUnion::DFUnion(){
		set_name("Union");
	}

	// constructor
	DFUnion::DFUnion(const ShDistFunPr &dfa, const ShDistFunPr &dfb) : DFUnion(){
		add_df(dfa); add_df(dfb);
	}

	// constructor
	DFUnion::DFUnion(const std::list<ShDistFunPr> &dflist) : DFUnion(){
		for(auto it=dflist.begin();it!=dflist.end();it++)add_df(*it);
	}

	// constructor
	DFUnion::DFUnion(const std::map<arma::uword, ShDistFunPr> &dflist) : DFUnion(){
		df_ = dflist;
	}

	// factory
	ShDFUnionPr DFUnion::create(){
		return std::make_shared<DFUnion>();
	}

	// factory
	ShDFUnionPr DFUnion::create(const ShDistFunPr &dfa, const ShDistFunPr &dfb){
		return std::make_shared<DFUnion>(dfa,dfb);
	}

	// factory
	ShDFUnionPr DFUnion::create(const std::list<ShDistFunPr> &dflist){
		return std::make_shared<DFUnion>(dflist);
	}

	// factory
	ShDFUnionPr DFUnion::create(const std::map<arma::uword, ShDistFunPr> &dflist){
		return std::make_shared<DFUnion>(dflist);
	}
	

	// add parts
	void DFUnion::add(const ShDistFunPr& df){
		add_df(df);
	}

	// add distance function
	arma::uword DFUnion::add_df(const ShDistFunPr &df){
		const arma::uword index = df_.size()+1;
		df_.insert({index,df}); return index;
	}
	
	// retreive distance function at index
	const ShDistFunPr& DFUnion::get_df(const arma::uword index) const{
		auto it = df_.find(index);
		if(it==df_.end())rat_throw_line("index does not exist");
		return (*it).second;
	}
	
	// delete distance function at index
	bool DFUnion::delete_df(const arma::uword index){	
		auto it = df_.find(index);
		if(it==df_.end())return false;
		(*it).second = NULL; return true;
	}

	// number of distance functions
	arma::uword DFUnion::num_df() const{
		return df_.size();
	}

	// re-index nodes after deleting
	void DFUnion::reindex(){
		std::map<arma::uword, ShDistFunPr> new_df; arma::uword idx = 1;
		for(auto it=df_.begin();it!=df_.end();it++)
			if((*it).second!=NULL)new_df.insert({idx++, (*it).second});
		df_ = new_df;
	}

	// get bounding box
	arma::Mat<fltp>::fixed<2,2> DFUnion::get_bounding() const{
		// check input
		is_valid(true);

		// get boxes of both
		arma::Mat<fltp>::fixed<2,2> M{1e99,-1e99,1e99,-1e99};

		// take overlapping area between two boxes
		for(auto it=df_.begin();it!=df_.end();it++){
			const arma::Mat<fltp>::fixed<2,2> Mt = (*it).second->get_bounding();
			M.row(0) = arma::min(M.row(0),Mt.row(0));
			M.row(1) = arma::max(M.row(1),Mt.row(1));
		}

		// return box
		return M;
	}

	// perimeter function
	ShPerimeterPr DFUnion::create_perimeter(const fltp delem) const{
		std::list<ShPerimeterPr> perimeters;
		for(auto it=df_.begin();it!=df_.end();it++)
			perimeters.push_back((*it).second->create_perimeter(delem));
		return Perimeter::create(perimeters);
	}

	// get fixed points
	arma::Mat<fltp> DFUnion::get_fixed(const fltp abstol) const{
		// check validity
		is_valid(true);

		// settings
		// const fltp delta = abstol/2;

		// size of the elements
		const fltp delem = calc_bounding_size()/64;

		// allocate fixed points
		arma::field<arma::Mat<fltp> > pfix_fld((df_.size()*df_.size() - df_.size())/2 + df_.size()); arma::uword cnt = 0;

		// walk over other distance functions
		for(auto it1=df_.begin();it1!=df_.end();it1++){
			// get distance function
			const ShDistFunPr& df1 = (*it1).second;

			// get perimeter of main object
			const ShPerimeterPr perimeter = df1->create_perimeter(delem);

			// walk over other objects
			for(auto it2=std::next(it1);it2!=df_.end();it2++){
				// get distance function
				const ShDistFunPr& df2 = (*it2).second;

				// find intersection points
				pfix_fld(cnt) = perimeter->intersect_df(df2);

				// refine their position using the distance functions
				// Perimeter::refine_intersection(pfix_fld(cnt),df1,df2,delta,abstol);
				
				// increment counter
				cnt++;
			}
		}

		// get fixed points from distance functions themselves
		for(auto it=df_.begin();it!=df_.end();it++)
			pfix_fld(cnt++) = (*it).second->get_fixed(abstol);

		// combine points
		arma::Mat<fltp> pfix = cmn::Extra::field2mat(pfix_fld);

		// find points that are actually on the boundary
		pfix.shed_rows(arma::find(arma::abs(calc_distance(pfix))>abstol));

		// combine points and return
		return pfix;
	}

	// distance function
	arma::Col<fltp> DFUnion::calc_distance(const arma::Mat<fltp> &p) const{
		// check if not empty
		is_valid(true);

		// calc distances to all shapes
		arma::Col<fltp> v(p.n_rows,arma::fill::value(1e99));
		for(auto it=df_.begin();it!=df_.end();it++)
			v = arma::min(v,(*it).second->calc_distance(p));

		// return smallest one
		return v;
	}

	// validity check
	bool DFUnion::is_valid(const bool enable_throws)const{
		// check user input
		if(df_.empty()){if(enable_throws){rat_throw_line("no distance functions set");} return false;};
		for(auto it=df_.begin();it!=df_.end();it++){
			if((*it).second==NULL){if(enable_throws){rat_throw_line("distance function points to NULL");} return false;};
			if(!(*it).second->is_valid(enable_throws))return false;
		}
		return true;
	}

	// type string for serialization
	std::string DFUnion::get_type(){
		return "rat::dm::dfunion";
	}

	// method for serialization into json
	void DFUnion::serialize(
		Json::Value &js, cmn::SList &list) const{

		// parent
		DistFun::serialize(js,list);

		// type
		js["type"] = get_type();
		
		// distance functions
		for(auto it=df_.begin();it!=df_.end();it++)
		 	js["distfuns"].append(cmn::Node::serialize_node((*it).second,list));
	}

	// method for deserialisation from json
	void DFUnion::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parent
		DistFun::deserialize(js,list,factory_list,pth);

		// create distance functions
		for(auto it = js["distfuns"].begin();it!=js["distfuns"].end();it++){
			const ShDistFunPr df = cmn::Node::deserialize_node<DistFun>(*it, list, factory_list, pth);
			if(df==NULL)rat_throw_line("could not deserialize distance function");
			add_df(df);
		}
	}

}}