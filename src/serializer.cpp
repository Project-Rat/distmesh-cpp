// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "serializer.hh"

// serializeable objects
#include "distmesh2d.hh"
#include "dfairfoil.hh"
#include "dfcircle.hh"
#include "dfdiff.hh"
#include "dfellipse.hh"
#include "dfintersect.hh"
#include "dfones.hh"
#include "dfpolygon.hh"
#include "dfrectangle.hh"
#include "dfrrectangle.hh"
#include "dfscale.hh"
#include "dftransform.hh"
#include "dfunion.hh"
#include "dfplasma.hh"
#include "dfarray.hh"
#include "dfarrayangular.hh"
#include "dfregpolygon.hh"

// code specific to Raccoon
namespace rat{namespace dm{
	
	// constructor
	Serializer::Serializer(){
		register_constructors();
	}

	// constructor
	Serializer::Serializer(const boost::filesystem::path &fname){
		register_constructors();
		import_json(fname);
	}

	// factory
	ShSerializerPr Serializer::create(){
		return std::make_shared<Serializer>();
	}

	// factory
	ShSerializerPr Serializer::create(const boost::filesystem::path &fname){
		return std::make_shared<Serializer>(fname);
	}

	// registry
	void Serializer::register_constructors(){
		// register all required constructors
		register_factory<DistMesh2D>();
		register_factory<DFAirFoil>();
		register_factory<DFArray>();
		register_factory<DFArrayAngular>();
		register_factory<DFCircle>();
		register_factory<DFDiff>();
		register_factory<DFEllipse>();
		register_factory<DFIntersect>();
		register_factory<DFOnes>();
		register_factory<DFPolygon>();
		register_factory<DFRectangle>();
		register_factory<DFRRectangle>();
		register_factory<DFScale>();
		register_factory<DFTransform>();
		register_factory<DFUnion>();
		register_factory<DFPlasma>();
		register_factory<DFRegPolygon>();
	}

}}
