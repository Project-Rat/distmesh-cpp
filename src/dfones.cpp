// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "dfones.hh"

// code specific to Rat
namespace rat{namespace dm{

	// constructor
	DFOnes::DFOnes(){
		set_name("Ones");
	}

	// factory
	ShDFOnesPr DFOnes::create(){
		return std::make_shared<DFOnes>();
	}

	// get bounding box
	arma::Mat<fltp>::fixed<2,2> DFOnes::get_bounding() const{
		// no real box
		arma::Mat<fltp>::fixed<2,2> Mb(arma::fill::zeros);

		// return box
		return Mb;
	}

	// distance function
	arma::Col<fltp> DFOnes::calc_distance(const arma::Mat<fltp> &p) const{
		return arma::Col<fltp>(p.n_rows,arma::fill::ones);
	}

	// type string for serialization
	std::string DFOnes::get_type(){
		return "rat::dm::dfones";
	}

	// method for serialization into json
	void DFOnes::serialize(
		Json::Value &js, cmn::SList &list) const{

		// parent
		DistFun::serialize(js,list);

		// type
		js["type"] = get_type();
	}


	// method for deserialisation from json
	void DFOnes::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		// parent
		DistFun::deserialize(js,list,factory_list,pth);
	}

}}