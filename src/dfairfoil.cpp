// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "dfairfoil.hh"

// code specific to Rat
namespace rat{namespace dm{

	// constructor
	DFAirFoil::DFAirFoil(){
		set_name("Air Foil");
	}

	// factory
	ShDFAirFoilPr DFAirFoil::create(){
		return std::make_shared<DFAirFoil>();
	}

	// set values
	void DFAirFoil::set_a(const arma::Col<fltp> &a){
		if(a.n_elem!=5)rat_throw_line("a must have five values");
		a_ = a;
	}

	// // perimeter function
	// ShPerimeterPr DFCircle::create_perimeter(const fltp delem) const{
	// 	arma::polyval(arma::join_vert(a_.rows(arma::regspace<arma::Col<arma::uword> >(4,1)), arma::Col<fltp>(1,arma::fill::zeros)));

	//  	// create perimeter
	// 	return Perimeter::create(Rn,n);
	// }

	// get bounding box
	arma::Mat<fltp>::fixed<2,2> DFAirFoil::get_bounding() const{
		// allocate
		arma::Mat<fltp>::fixed<2,2> Mb;
		
		// bounding in x
		Mb.col(0) = arma::Col<fltp>::fixed<2>{0.0,1.0};
		
		// bounding in y
		Mb.col(1) = arma::Col<fltp>::fixed<2>{-0.1,0.1};

		// return box
		return Mb;
	}

	// get fixed points
	arma::Mat<fltp> DFAirFoil::get_fixed(const fltp /*abstol*/) const{
		return arma::Row<fltp>{1,0};
	}

	// distance function
	arma::Col<fltp> DFAirFoil::calc_distance(const arma::Mat<fltp> &p) const{
		return arma::square(arma::abs(p.col(1))-arma::polyval(arma::join_vert(
			a_.rows(arma::regspace<arma::Col<arma::uword> >(4,1)),
			arma::Col<fltp>(1,arma::fill::zeros)),
			p.col(0)))-a_(0)*a_(0)*p.col(0);
	}

	// validity check
	bool DFAirFoil::is_valid(const bool enable_throws)const{
		// check user input
		if(a_.n_elem!=5){if(enable_throws){rat_throw_line("a must have five values");} return false;};
		return true;
	}

	// type string for serialization
	std::string DFAirFoil::get_type(){
		return "rat::dm::dfairfoil";
	}
	
	// method for serialization into json
	void DFAirFoil::serialize(
		Json::Value &js, cmn::SList &list) const{
		
		// parent
		DistFun::serialize(js,list);

		// store type ID
		js["type"] = get_type();
		js["a0"] = a_(0); js["a1"] = a_(1); js["a2"] = a_(2);
		js["a3"] = a_(3); js["a4"] = a_(4);	
	}

	// method for deserialisation from json
	void DFAirFoil::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parent
		DistFun::deserialize(js,list,factory_list,pth);

		// get values
		a_.set_size(5);
		a_(0) = js["a0"].ASFLTP(); a_(1) = js["a1"].ASFLTP();
		a_(2) = js["a2"].ASFLTP(); a_(3) = js["a3"].ASFLTP(); 
		a_(4) = js["a4"].ASFLTP();
	}
	
}}