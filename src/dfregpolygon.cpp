// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "dfregpolygon.hh"

// distmesh-cpp headers
#include "dfunion.hh"
#include "dfcircle.hh"
#include "dfpolygon.hh"

// code specific to Rat
namespace rat{namespace dm{

	// constructor
	DFRegPolygon::DFRegPolygon(){
		set_name("Regular Polygon");
	}

	// constructor
	DFRegPolygon::DFRegPolygon(
		const arma::uword num_sides, 
		const fltp alpha, 
		const fltp radius) : DFRegPolygon(){
		
		// set parameters
		set_num_sides(num_sides);
		set_alpha(alpha); 
		set_radius(radius);
	}


	// factory
	ShDFRegPolygonPr DFRegPolygon::create(){
		return std::make_shared<DFRegPolygon>();
	}

	// factory with dimensional input
	ShDFRegPolygonPr DFRegPolygon::create(
		const arma::uword num_sides, 
		const fltp alpha,
		const fltp radius){
		return std::make_shared<DFRegPolygon>(num_sides, alpha, radius);
	}


	// setters
	void DFRegPolygon::set_radius(const fltp radius){
		radius_ = radius;
	}

	void DFRegPolygon::set_alpha(const fltp alpha){
		alpha_ = alpha;
	}

	void DFRegPolygon::set_num_sides(const arma::uword num_sides){
		num_sides_ = num_sides;
	}


	// getters
	fltp DFRegPolygon::get_radius()const{
		return radius_;
	}

	fltp DFRegPolygon::get_alpha()const{
		return alpha_;
	}

	arma::uword DFRegPolygon::get_num_sides()const{
		return num_sides_;
	}


	// calculate length of the sides
	fltp DFRegPolygon::calc_side_length() const{
		return alpha_*2*std::tan(arma::Datum<fltp>::pi/num_sides_);
	}

	// calculate length of arc in rad
	fltp DFRegPolygon::calc_arc_length() const{
		return arma::Datum<fltp>::pi/num_sides_;
	}

	// calculate corner length
	fltp DFRegPolygon::calc_corner_length() const{
		return radius_*std::tan(calc_arc_length()); 
	}
	

	// create distance function
	ShDistFunPr DFRegPolygon::create_distfun()const{
		// length of each side
		const fltp side_length = calc_side_length();

		// angle in corner
		// const fltp arc_length = calc_arc_length();

		// Calculate the lengths that need to be subtracted
		// from the total to compensate for the rounded corners
		// const fltp corner_length = calc_corner_length();

		// outer radius
		const fltp alpha2 = std::hypot(alpha_, side_length/2);

		// circle radius
		const fltp alpha_circle = alpha2 - radius_;

		
		// rounded corner case
		if(radius_>RAT_CONST(0.0)){
			// create a combined distance function
			const ShDFUnionPr distfun = DFUnion::create();

			// create list of points to create the straight sections
			arma::Mat<fltp> p(2*num_sides_,2);

			// circles at corner locations
			for(arma::uword i=0;i<num_sides_;i++){
				// calculate circle center
				const fltp theta = i*arma::Datum<fltp>::tau/num_sides_ + arma::Datum<fltp>::tau/num_sides_/2;
				const fltp yc = alpha_circle*std::sin(theta);
				const fltp xc = alpha_circle*std::cos(theta);

				// add the circle
				distfun->add(DFCircle::create(radius_,xc,yc));

				// first point
				p(2*i,0) = xc + radius_*std::cos(theta - arma::Datum<fltp>::tau/num_sides_/2);
				p(2*i,1) = yc + radius_*std::sin(theta - arma::Datum<fltp>::tau/num_sides_/2);

				// second point
				p(2*i+1,0) = xc + radius_*std::cos(theta + arma::Datum<fltp>::tau/num_sides_/2);
				p(2*i+1,1) = yc + radius_*std::sin(theta + arma::Datum<fltp>::tau/num_sides_/2);
			}

			// add the polygon
			distfun->add(DFPolygon::create(p));


			// return the union
			return distfun;
		}

		// sharp corner case
		else{
			// circles at corner locations
			const arma::Col<fltp> theta = arma::linspace<arma::Col<fltp> >(RAT_CONST(0.0), arma::Datum<fltp>::tau, num_sides_ + 1) + arma::Datum<fltp>::tau/num_sides_/2;
			const arma::Mat<fltp> p = alpha2*arma::join_horiz(arma::cos(theta),arma::sin(theta));
			return DFPolygon::create(p);
		}
	}

	// perimeter function
	ShPerimeterPr DFRegPolygon::create_perimeter(const fltp delem) const{
		return create_distfun()->create_perimeter(delem);
	}

	// get bounding box
	arma::Mat<fltp>::fixed<2,2> DFRegPolygon::get_bounding() const{
		return create_distfun()->get_bounding();
	}

	// get fixed points
	arma::Mat<fltp> DFRegPolygon::get_fixed(const fltp abstol) const{
		return create_distfun()->get_fixed(abstol);
	}

	// distance function
	arma::Col<fltp> DFRegPolygon::calc_distance(const arma::Mat<fltp> &p) const{
		return create_distfun()->calc_distance(p);
	}

	// validity check
	bool DFRegPolygon::is_valid(const bool enable_throws)const{
		// check user input
		if(radius_<0){if(enable_throws){rat_throw_line("radius can not be negative");} return false;};
		if(alpha_<=0){if(enable_throws){rat_throw_line("alpha must be larger than zero");} return false;};
		if(num_sides_<3){if(enable_throws){rat_throw_line("num sides must be at least three");} return false;};
		if(radius_>calc_side_length()/2){if(enable_throws){rat_throw_line("radius must be smaller than side length");} return false;};
		return true;
	}

	// type string for serialization
	std::string DFRegPolygon::get_type(){
		return "rat::dm::dfregpolygon";
	}

	// method for serialization into json
	void DFRegPolygon::serialize(
		Json::Value &js, cmn::SList &list) const{
		// parent
		DistFun::serialize(js,list);

		// store type ID
		js["type"] = get_type();
		
		// properties
		js["radius"] = radius_; 
		js["alpha"] = alpha_;
		js["num_sides"] = static_cast<int>(num_sides_);	
	}

	// method for deserialisation from json
	void DFRegPolygon::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		
		// parent
		DistFun::deserialize(js,list,factory_list,pth);

		// get properties
		set_radius(js["radius"].ASFLTP()); 
		set_alpha(js["alpha"].ASFLTP());
		set_num_sides(js["num_sides"].asUInt64()); 
	}

}}