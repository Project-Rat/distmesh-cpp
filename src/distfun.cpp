// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "distfun.hh"

// code specific to Rat
namespace rat{namespace dm{

	// create perimeter
	ShPerimeterPr DistFun::create_perimeter(const fltp /*delem*/)const{
		return Perimeter::create(arma::Mat<fltp>(2,0),arma::Mat<arma::uword>(2,0));
	}

	// calculate typical size
	fltp DistFun::calc_bounding_size()const{
		// estimate element size from bounding box
		const arma::Mat<fltp>::fixed<2,2> M = get_bounding();
		const arma::Row<fltp>::fixed<2> size = M.row(1) - M.row(0);

		// size of the bounding box
		return arma::max(size);
	}

	// get fixed points
	arma::Mat<fltp> DistFun::get_fixed(const fltp /*abstol*/) const{
		// no fixed points
		return arma::Mat<fltp>(0,2);
	}

	// type string for serialization
	std::string DistFun::get_type(){
		return "rat::dm::distfun";
	}

	// method for serialization into json
	void DistFun::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		Node::serialize(js,list);

		// store type
		js["type"] = get_type();

	}

	// method for deserialisation from json
	void DistFun::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parent
		Node::deserialize(js,list,factory_list,pth);
	}

}}