![Logo](./figs/RATLogo.png)
# Distmesh-Cpp
<em>Meshing is an art. Rat likes art yes?</em>

## Introduction
In this repository you find a C++ version of the famous distmesh mesher originally written in Matlab by P.-O. Persson [available here](http://persson.berkeley.edu/distmesh/) \[1,2\]. Currently only two dimensional meshes are implemented. The delaunay triangulation algorithm is translated from triangulate originally written by Paul Bourke [available here](http://paulbourke.net/papers/triangulate/) \[3\]. This code uses the [Armadillo](http://arma.sourceforge.net/) library for all linear algebra operations. If your project relies on [Eigen](http://eigen.tuxfamily.org) instead you may want to consider the C++ implementation by P. Gebhardt [available here](https://github.com/pgebhardt/libdistmesh). 

In addition to the original distmesh code, a recombination algorithm has been added that combines the triangles into quadrilaterals. Then with the Catmull Clark subdivision algorithm the left-over triangles are split unto quadrilaterals as well. This allows for the generation of pure quad meshes. The code is used in Rat for the generation of the cross-sectional elements of any cable or coil, which will then be extruded along its length. In addition the code will be used for the meshing of the meshes for non-linear materials.

![trimesh and quadmesh](./figs/triquad.png)

## Installation
As this library is part of a collection of inter-dependent libraries a detailed description of the installation process is provided in a separate repository located here: [rat-docs](https://gitlab.com/Project-Rat/rat-documentation). It is required that you install the [rat-common](https://gitlab.com/Project-Rat/rat-common) library before installing this. A quick build and installation is achieved by using

```bash
mkdir release
cd release
cmake ..
make -jX
```

where X is the number of cores you wish to use for the build. When the library is build run unit tests and install using

```bash
make test
sudo make install
```

## Meshing With Distance Functions
The mesh is generated using a distance function and optionally a scaling function. The distance function calculates the distance between any provided point and the mesh boundary. For the C++ implementation it was found more convenient to describe the distance function using a graph of objects instead of functions (i.e. using std::functional). Each node in the graph represents either a basic shape, for example a circle or a rectangle, or an operation such as addition (union), intersection (intersect) or subtraction (diff). For example to mesh a rectangle with a circular hole, one can use the following example code

```cpp
// create distmesher object
rat::dm::ShDistMesh2DPr mymesh = rat::dm::DistMesh2D::create();

// set the initial edge length (element size)
mymesh->set_h0(0.05);

// build the distance function out of objects
mymesh->set_distfun(rat::dm::DFDiff::create(
	rat::dm::DFRectangle::create(-1,1,-1,1),
	rat::dm::DFCircle::create(0.5,0,0)));

// setup mesh
mymesh->setup();

// get node coordinates and elements
arma::Mat<double> p = mymesh->get_nodes();
arma::Mat<arma::uword> q = mymesh->get_elements();
```

Here the difference function subtracts the circle from the rectangle. The input for the rectangle are its boundaries (x1,x2,y1,y2) and the input for the circle is the radius and center position (r,xc,yc). When the setup method is called the mesh is generated. After which the nodes and the elements can be extracted. Further examples are available in the examples folder.

The objects describing distance functions start with DF and current available functions are:

* **DFAirfoil** - Describes an airfoil (from P.-O. Persson example). It is required to set the input *a* which defines the shape of the polynomial, used to describe the surface of the airfoil.
* **DFCircle** - Describes a circle. The inputs are the radius *r* and the coordinates of its center *xc* and *yc*, respectively. 
* **DFEllipse** - Describes an ellipse. The inputs are its dimensions *a* and *b* and its center *xc* and *yc*, respectively. 
* **DFIntersect** - Takes two distance functions at its input and converts it to the distance function for a common region between the two. 
* **DFOnes** - Distance function outputting one everywhere. This is used for scaling function to not have the mesh size depend on the distance to the boundary. 
* **DFPolygon** - Describes a polygon using a list of vertices *v = [x,y]* and calculates the distance function. 
* **DFRectangle** - Describes a rectangle using its spans horizontally *x1* and *x2* and vertically *y1* and *y2*. 
* **DFRRectangle** - Same as DFRectangle but with rounded corners. Additionally requires the radius *r*. 
* **DFScale** - Scales a distance function with *scale* offsets it by *offset* and caps it by *max*. This is used to controll the element size when used in a scaling function.
* **DFUnion** - Takes multiply distance functions at its input and converts it to the distance function for all regions. 

If you wish to implement your own distance function you could do this by inheriting from DistFun and implementing at least its pure virtual functions.

## High valence removal
The mesh quality can be improved by the high valence removal feature. See the figure below where a high valence (5 or 6) node (green) gets split into two lower valence nodes (pink). 

![high valence removal](./figs/high_valence_removal.png)

## Gallery
Using the distance functions almost any shape can be meshed see some examples in the figure below.
![basic meshes](./figs/basicshapes.png)

The code is to be used for meshing the iron regions for non-linear magnetic field calculation. In the figure below you see a few two-dimensional cross sections of commonly used iron yokes. These cross sections will be extruded to form the three-dimensional mesh.
![yoke shapes](./figs/yokes.png)

## Authors
* Jeroen van Nugteren
* Nikkie Deelen

## License
This project is licensed under the [MIT](LICENSE). Permission to release this adaptation under MIT license was given by Per-Olof Persson on December 23rd, 2021 by e-mail. Many thanks to Per-Olof Persson for this.

## References
[1] P.-O. Persson, G. Strang, "A Simple Mesh Generator in MATLAB"
SIAM Review, Volume 46 (2), pp. 329-345, June 2004.

[2] P.-O. Persson, "Mesh Generation for Implicit Geometries",
Ph.D. thesis, Department of Mathematics, MIT, Dec 2004.

[3] P.D. Bourke, "A Contouring Subroutine", BYTE Magazine, June, 1987.