// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <armadillo>

#include "rat/common/error.hh"
#include "distmesh2d.hh"
#include "dfcircle.hh"

// main
int main(){	
	// the mesh was drawn by hand and is just imported here as a table
	const arma::Mat<rat::fltp> R1 = arma::reshape(arma::Mat<rat::fltp>{
		0.0,-1.0, //    #00
		0.5,0.0, //     #01
		0.0,1.0, //     #02
		1.0,-1.5, //    #03
		1.5,-1.0, //    #04
		2.0,-1.0, //    #05
		2.2,0.0, //     #06
		2.0,1.0, //     #07
		1.5,1.0, //     #08
		0.5,2.0, //     #09
		-1.0,-1.0, //   #10
		-1.5,0.0, //    #11
		-1.0,1.0, //    #12
		-2.0,-2.0, //   #13
		-2.5,-1.5, //   #14
		-3.2,-0.5, //   #15
		-3.0,0.0, //    #16
		-3.2,0.5, //    #17
		-2.5,1.0, //    #18
		-2.0,1.5 //     #19
		},2,20)*0.01; // convert from cm to m
	const arma::Mat<arma::uword> n1 = arma::reshape(arma::Mat<arma::uword>{
		11,10,0,1, //   #00
		1,0,3,4, //     #01
		1,4,5,6, //     #02
		1,6,7,8, //   #03
		1,8,9,2, //   #04
		12,11,1,2, //   #05
		18,11,12,19, // #08
		17,16,11,18, // #09
		16,15,14,11, // #10
		14,13,10,11 //  #11
		},4,10);

	// settings
	rat::dm::ShDistMesh2DPr dm = rat::dm::DistMesh2D::create();
	
	// set the mesh directly
	dm->set_nodes(R1.t());
	dm->set_elements(n1.t());
	
	// // remove tha high valence nodes
	dm->create_edges();

	// check that we indeed have valence 6 nodes in this test mesh
	const arma::Col<arma::uword> valence1 = dm->get_node_valence();
	if(arma::accu(valence1==6)!=2)rat_throw_line("there should be 2 valence 6 nodes");

	// try remove all valence 6 nodes from the mesh
	dm->high_valence_removal(6);

	// valence should not have changed as elements are overlapping
	const arma::Col<arma::uword> valence2 = dm->get_node_valence();
	if(arma::any(valence2>6))rat_throw_line("valence has increased");
	if(arma::accu(valence2==6)!=1)rat_throw_line("there should be 1 valence 6 nodes");

	// try remove all valence 6 nodes from the mesh
	dm->high_valence_removal(6);

	// valence should not have changed as elements are overlapping
	const arma::Col<arma::uword> valence3 = dm->get_node_valence();
	if(arma::any(valence3>6))rat_throw_line("valence has increased");
	if(arma::any(valence3==6))rat_throw_line("there should be no valence 6 nodes left");

	// check mesh integrity
	const arma::Mat<arma::uword>& n2 = dm->get_elements().t();
	const arma::Mat<rat::fltp>& R2 = dm->get_nodes().t();

	// check if the largest node exists
	if(n2.max()>=R2.n_cols)rat_throw_line("mesh integrity check failed");
	for(arma::uword i=0;i<n2.n_cols;i++){
		if(arma::unique(n2.col(i)).eval().n_elem!=n2.n_rows)rat_throw_line("element contains duplicate nodes");
	}

	// smooth the mesh
	dm->smoothen();

	// write to gmsh
	dm->export_gmsh(rat::cmn::GmshFile::create("valence_removal.gmsh"));

	// end
	return 0;
}