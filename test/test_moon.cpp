// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <armadillo>

#include "distmesh2d.hh"
#include "dfcircle.hh"
#include "dfdiff.hh"

// main
int main(){
	// settings
	const rat::fltp radius1 = RAT_CONST(1.0);
	const rat::fltp radius2 = RAT_CONST(0.5);
	const rat::fltp tol = RAT_CONST(1e-2);

	// derived constants
	const rat::fltp offsetx = radius2-RAT_CONST(0.1);
	const rat::fltp element_size = radius1/10;

	// theoretical area
	const rat::fltp Ath = arma::Datum<rat::fltp>::pi*(radius1*radius1-radius2*radius2);

	// try both quad an tri-meshes
	for(arma::uword quad=0;quad<2;quad++){
		// create mesh
		rat::dm::ShDistMesh2DPr mymesh = rat::dm::DistMesh2D::create();
		mymesh->set_quad(quad);

		// initial edge length
		mymesh->set_h0(element_size);

		// the distance function
		mymesh->set_distfun(rat::dm::DFDiff::create(
			rat::dm::DFCircle::create(radius1,0,0),
			rat::dm::DFCircle::create(radius2,offsetx,0)));
	
		// setup mesh
		mymesh->setup();

		// calculate mesh area
		const rat::fltp Amsh = mymesh->calc_area();

		// compare
		if(std::abs(Ath-Amsh)/Ath>tol){
			rat_throw_line("calculated mesh area does not match theory");
		}
	}

	// end
	return 0;
}