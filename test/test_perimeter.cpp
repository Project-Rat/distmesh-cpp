// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <armadillo>

#include "rat/common/error.hh"

#include "dfcircle.hh"
#include "dfdiff.hh"

// main
int main(){	
	// settings
	const rat::fltp radius = RAT_CONST(0.7);
	const rat::fltp separation = 0.3;
	const rat::fltp element_size = radius/10;
	const rat::fltp tol = 1e-6;

	// the distance function
	const rat::dm::ShDistFunPr distfun = rat::dm::DFDiff::create(
		rat::dm::DFCircle::create(radius,-separation/2,0),
		rat::dm::DFCircle::create(radius,+separation/2,0));
	
	// get intersection points between two circles
	const arma::Mat<rat::fltp> Pfix = distfun->get_fixed(tol);

	// calculate circle to circle intersection point coordinate analytically
	const rat::fltp y = std::sin(std::acos((separation/2)/radius))*radius;

	std::cout<<Pfix<<std::endl;

	// check output
	if(arma::any(arma::abs(Pfix.col(0))>tol))
		rat_throw_line("intersection point x-coord outside of tolerance");
	if(arma::any(arma::abs(arma::abs(Pfix.col(1)) - y)>tol))
		rat_throw_line("intersection point y-coord outside of tolerance");

	// end
	return 0;
}