// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <armadillo>

#include "distmesh2d.hh"
#include "dfcircle.hh"
#include "dfrectangle.hh"
#include "dfscale.hh"
#include "dfdiff.hh"

// main
int main(){
	// settings
	const rat::fltp hole_radius = RAT_CONST(0.5);
	const rat::fltp square_sides = RAT_CONST(2.0);
	const rat::fltp min_elements = RAT_CONST(0.05);
	const rat::fltp scale = RAT_CONST(0.3);
	const rat::fltp tol = RAT_CONST(1e-2);
	const rat::fltp h0 = RAT_CONST(0.05);

	// create mesh
	rat::dm::ShDistMesh2DPr mymesh = rat::dm::DistMesh2D::create();

	// initial edge length
	mymesh->set_h0(h0);

	// the distance function
	mymesh->set_distfun(rat::dm::DFDiff::create(rat::dm::DFRectangle::create(-1,1,-1,1),rat::dm::DFCircle::create(0.5,0,0)));

	// the scale function
	mymesh->set_scalefun(rat::dm::DFScale::create(rat::dm::DFCircle::create(0.5,0,0),0.05,0.3));
		

	// theoretical area
	const rat::fltp Ath = square_sides*square_sides - arma::Datum<rat::fltp>::pi*hole_radius*hole_radius;

	// try both quad an tri-meshes
	for(arma::uword quad=0;quad<2;quad++){
		// create mesh
		rat::dm::ShDistMesh2DPr mymesh = rat::dm::DistMesh2D::create();
		mymesh->set_quad(quad);

		// initial edge length
		mymesh->set_h0(min_elements);

		// the distance function
		mymesh->set_distfun(rat::dm::DFDiff::create(
			rat::dm::DFRectangle::create(-square_sides/2,square_sides/2,-square_sides/2,square_sides/2),
			rat::dm::DFCircle::create(hole_radius,0,0)));

		// the scale function
		mymesh->set_scalefun(rat::dm::DFScale::create(rat::dm::DFCircle::create(hole_radius,0,0),min_elements,scale));

		// setup mesh
		mymesh->setup();

		// calculate mesh area
		const rat::fltp Amsh = mymesh->calc_area();

		// compare
		if(std::abs(Ath-Amsh)/Ath>tol){
			rat_throw_line("calculated mesh area does not match theory");
		}
	}



	return 0;
}